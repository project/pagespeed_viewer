<?php

namespace Drupal\Tests\pagespeed_viewer\Unit;

use Drupal\pagespeed_viewer\Model\PageSpeedModel;
use Drupal\Tests\UnitTestCase;

/**
 * Unit tests for the PageSpeedModel utility class.
 *
 * @group PageSpeedModel
 *
 * @coversDefaultClass \Drupal\pagespeed_viewer\Model\PageSpeedModel
 */
class PageSpeedModelTest extends UnitTestCase {

  /**
   * {@inheritdoc}
   */
  public function setUp() {
  }

  /**
   * Test the calculateAverage Method.
   */
  public function testCalculateAverage() {
    $expected = 75;
    $model = new PageSpeedModel(100, 50);
    $result = $model->calculateAverage();
    $this::assertEquals($expected, $result);
  }

}
