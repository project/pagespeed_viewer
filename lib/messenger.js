/*
* Add HTML element to parent tag.
* */
function addElement(parentId, elementTag, html) {
  var p = document.getElementById(parentId);
  var newElement = document.createElement(elementTag);
  newElement.innerHTML = html;
  if(!p.hasChildNodes()){
    p.appendChild(newElement);
  }
}
/*
* Remove HTML element.
* */
function removeElement(elementId) {
  var element = document.getElementById(elementId);
  element.parentNode.removeChild(element);
}
/*
* Display error message
* */
function showMessage(msg){
  var html = '<div class="alert alert-warning" role="alert">' + msg + '</div>';
  addElement("error_msg","div",html);
}
