/*
 * Get data from backend to draw chart.
 */
(function ($, Drupal, drupalSettings) {
  'use strict';
  Drupal.behaviors.pagespeed_viewer = {
    attach: function (context, settings) {
      var statistics = Object.values(drupalSettings.pagespeed_viewer.statistics);
      if(statistics.length !== 0){
        statistics.sort((a, b) => new Date(b.created_at) - new Date(a.created_at));
        var data = {
          created: [],
          mobile: [],
          avg: [],
          desktop: [],
        };
        for(var val of Object.values(statistics)) {
          data.created.push(val.created_at);
          data.mobile.push(val.mobile);
          data.avg.push(val.avg);
          data.desktop.push(val.desktop);
        }
        drawChart(data);
      }
      else {
        showMessage('There are no data in the database available');
      }
    }
  };
})(jQuery, Drupal, drupalSettings);
/*
 * Visualize statistics data with bar chart.
 */
function drawChart(data){
  new Chart(document.getElementById('myChart').getContext('2d'), {
    type: 'bar',
    data: {
      labels: data.created,
      datasets: [
        {
          label: "Mobile",
          backgroundColor: "#2BBCD7",
          data: data.mobile,
        }, {
          label: "AVG",
          backgroundColor: "#15CA7B",
          data: data.avg,
        },
        {
          label: "Desktop",
          backgroundColor: "#D39C49",
          data: data.desktop,
        }
      ]
    },
    options: {
      title: {
        display: true,
        text: 'Last ' + data.created.length + ' measured Pagespeed values'
      },
      scales: {
        yAxes: [{
          ticks: {
            beginAtZero: true
          }
        }]
      }
    }
  });
}
