/*
 * Set click event for button.
 */
(function ($, Drupal, drupalSettings) {
  'use strict';
  Drupal.behaviors.fetchOnClick = {
    attach: function (context, settings) {
      if (drupalSettings.pagespeed_viewer.url !== null) {
        $('#fetchdata_btn').click(function () {
          fetchdata(drupalSettings.pagespeed_viewer.apiUrl);
        });
      }
      else {
      showMessage("Page URL should be configured to measure page speed.");
      }
    }
  };
})(jQuery, Drupal, drupalSettings);
/*
 * Get data from backend to draw chart
 */
function fetchdata(apiUrl){
  removeElement("fetchdata_btn");
  showLoader(true);
  axios.get(apiUrl)
    .then(function (response) {
      var cards = document.getElementsByClassName('card');
      var chartData = {
        container: [],
        color: ['#2BBCD7','#15CA7B','#D39C49'],
        results: [],
      };
      for(var [key, value] of Object.entries(response.data[0])) {
        chartData.container.push(key);
        chartData.results.push(value);
      }
      showLoader(false);
      Array.from(cards).forEach((card) => {
        card.style.display = 'block';
      });
      initCharts(chartData);
    })
     .catch(function (error) {
       showMessage('Something went wrong during communicating with the API. Try to refresh the page or try again later...');
       showLoader(false);
     });
}
/*
 * Define different types of charts (element/color/value).
 */
function initCharts(chartData) {
  for (var i = 0; i < chartData.container.length; i++) {
    drawChart(chartData.container[i], chartData.color[i], chartData.results[i]);
  }
}
/*
 * Draw defined charts.
 */
function drawChart(container, color, res) {
  var bar = new ProgressBar.Circle(document.getElementById(container), {
    color: color,
    strokeWidth: 4,
    trailWidth: 1,
    easing: 'easeInOut',
    duration: 1400,
    text: {
      autoStyleContainer: false
    },
    from: { color: color, width: 1 },
    to: { color: color, width: 4 },

    step: function (state, circle) {
      circle.path.setAttribute('stroke', state.color);
      circle.path.setAttribute('stroke-width', state.width);

      var value = Math.round(circle.value() * 100);
      if (value === 0) {
        circle.setText('');
      } else {
        circle.setText(value);
      }
    }
  });
  bar.text.style.fontFamily = '"Raleway", Helvetica, sans-serif';
  bar.text.style.top = '46%';
  bar.text.style.fontSize = '2rem';
  bar.animate(res / 100);
}
/*
 * Toggle loader visibility.
 */
function showLoader(condition) {
  if(condition) {
    document.getElementById("overlay").style.display = "block";
    document.getElementById("loader").style.visibility = "visible";
  }else {
    document.getElementById("overlay").style.display = "none";
    document.getElementById("loader").style.visibility = "hidden";
  }
}
