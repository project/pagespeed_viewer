<?php

namespace Drupal\pagespeed_viewer\Model;

/**
 * Class PageSpeedModel.
 *
 * @package Drupal\pagespeed_viewer\Model
 */
class PageSpeedModel {
  /**
   * Mobile page speed.
   *
   * @var float
   */
  private $mobile;
  /**
   * Desktop page speed.
   *
   * @var float
   */
  private $desktop;

  /**
   * PageSpeedModel constructor.
   */
  public function __construct($desktop, $mobile) {
    $this->desktop = $desktop;
    $this->mobile = $mobile;
  }

  /**
   * Getter.
   *
   * @return float
   *   Mobile page speed.
   */
  public function getMobile() {
    return $this->mobile;
  }

  /**
   * Setter.
   *
   * @param float $mobile
   *   Mobile page speed.
   */
  public function setMobile($mobile) {
    $this->mobile = $mobile;
  }

  /**
   * Getter.
   *
   * @return float
   *   Desktop page speed.
   */
  public function getDesktop() {
    return $this->desktop;
  }

  /**
   * Setter.
   *
   * @param float $desktop
   *   Desktop page speed.
   */
  public function setDesktop($desktop) {
    $this->desktop = $desktop;
  }

  /**
   * Calculate average page speed.
   *
   * @return float
   *   Average page speed.
   */
  public function calculateAverage() {
    $sum = $this->mobile + $this->desktop;
    return round($sum / 2, 2);
  }

}
