<?php

namespace Drupal\pagespeed_viewer\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class PagespeedViewerController.
 *
 * @package Drupal\pagespeed_viewer\Controller
 */
class PagespeedViewerController extends ControllerBase {

  /**
   * Pagespeed API Service.
   *
   * @var \Drupal\pagespeed_viewer\PagespeedApiService
   */
  protected $pagespeedApi;
  /**
   * Pagespeed DB Service.
   *
   * @var \Drupal\pagespeed_viewer\PageSpeedDBService
   */
  protected $pagespeedDB;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->pagespeedApi = $container->get('pagespeed_api');
    $instance->pagespeedDB = $container->get('pagespeed_db');
    return $instance;
  }

  /**
   * Renders index page.
   *
   * @return array
   *   Index page
   */
  public function init() {
    $url = $this->pagespeedApi->getUrl();
    $apiUrl = Url::fromRoute('pagespeed_viewer.api')->toString();
    $build = [];
    $build['content'] = [
      '#theme' => 'pagespeed_viewer',
      '#url' => $url,
      '#categories' => ['mobile', 'avg', 'desktop'],
    ];
    $build['#attached']['library'][] = 'pagespeed_viewer/pagespeed_style_overview';
    $build['#attached']['drupalSettings']['pagespeed_viewer']['apiUrl'] = $apiUrl;
    $build['#attached']['drupalSettings']['pagespeed_viewer']['url'] = $url;
    return $build;
  }

  /**
   * Renders overview page.
   *
   * @return array
   *   Overview page
   */
  public function getStatistics() {
    $statistics = $this->pagespeedDB->getStatistics();
    $lastAddedStatistics = $this->pagespeedDB->getLastFiveAddedStatistics();
    $build = [];
    $build['content'] = [
      '#theme' => 'pagespeed_viewer_statistics',
      '#statistics' => $statistics,
    ];
    $build['#attached']['library'][] = 'pagespeed_viewer/pagespeed_style_statistics';
    $build['#attached']['drupalSettings']['pagespeed_viewer']['statistics'] = $lastAddedStatistics;
    return $build;
  }

  /**
   * Provides JSON of pagespeed data.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   A JSON File of measured Pagespeed.
   */
  public function getPagespeedJson() {
    $data = $this->pagespeedApi->savePagespeed();
    $json = ['error' => TRUE];
    if ($data != FALSE) {
      $url = $this->pagespeedApi->getURL();
      $this->pagespeedDB->saveStatisitcs($url, $data['desktop'], $data['mobile'], $data['avg']);
      $json = [
        'mobile' => $data['mobile'],
        'avg' => $data['avg'],
        'desktop' => $data['desktop'],
      ];
    }
    return new JsonResponse([$json], 200);
  }

}
