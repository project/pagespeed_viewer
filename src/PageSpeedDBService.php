<?php

namespace Drupal\pagespeed_viewer;

use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\pagespeed_viewer\Entity\PagespeedData;

/**
 * Class PageSpeedDBService.
 *
 * @package Drupal\pagespeed_viewer
 */
class PageSpeedDBService {
  /**
   * Logger Service.
   *
   * @var Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $logger;

  /**
   * PageSpeedDBService constructor.
   */
  public function __construct(LoggerChannelFactoryInterface $loggerChannelFactory) {
    $this->logger = $loggerChannelFactory->get('pagespeed_viewer');
  }

  /**
   * Saves the measured pagespeed into the DB.
   *
   * @param string $url
   *   URL of the page.
   * @param float $desktop
   *   Desktop pagespeed value.
   * @param float $mobile
   *   Mobile pagespeed value.
   * @param float $avg
   *   Average pagespeed value.
   */
  public function saveStatisitcs($url, $desktop, $mobile, $avg) {
    $statistic = PagespeedData::create([
      'url' => $url,
      'mobile' => $mobile,
      'desktop' => $desktop,
      'avg' => $avg,
    ]);
    try {
      $statistic->save();
    }
    catch (EntityStorageException $e) {
      $this->logger->error($e->getMessage());
    }
  }

  /**
   * Converts statistics values to an array.
   *
   * @param array $ids
   *   List of ids of queried statistics.
   *
   * @return array
   *   Array of statistic values
   */
  public function getStatisticsArray(array $ids) {
    $entities = PagespeedData::loadMultiple($ids);
    $statistics = [];
    foreach ($entities as $e) {
      $id = $e->id();
      $statistics[$id]['url'] = $e->getUrl();
      $statistics[$id]['mobile'] = $e->getMobile();
      $statistics[$id]['desktop'] = $e->getDesktop();
      $statistics[$id]['avg'] = $e->getAvg();
      $statistics[$id]['created_at'] = $e->getCreated();
    }
    return $statistics;
  }

  /**
   * Get every statistics data from db.
   *
   * @return array
   *   Array of statistics
   */
  public function getStatistics() {
    $ids = \Drupal::entityQuery('pagespeed_data')
      ->sort('created_at', 'DESC')
      ->execute();
    return $this->getStatisticsArray($ids);
  }

  /**
   * Get last 5 added statistics data from DB.
   *
   * @return array
   *   Array of statistics
   */
  public function getLastFiveAddedStatistics() {
    $ids = \Drupal::entityQuery('pagespeed_data')
      ->sort('created_at', 'DESC')
      ->range(0, 5)
      ->execute();
    return $this->getStatisticsArray($ids);
  }

}
