<?php

namespace Drupal\pagespeed_viewer\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;

/**
 * Class URLConfigForm.
 *
 * @package Drupal\pagespeed_viewer\Form
 */
class URLConfigForm extends ConfigFormBase {
  /**
   * Name of defined config file.
   *
   * @var string
   */
  const CONFIG = 'pagespeed_viewer.urlconfig';

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'urlconfig_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::CONFIG,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $config = $this->config(static::CONFIG);

    $form['url'] = [
      '#type' => 'url',
      '#title' => $this->t('URL:'),
      '#description' => $this->t('Type a page URL which you would like to test'),
      '#default_value' => $config->get('url'),
      '#weight' => '0',
    ];
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
    try {
      $client = new Client();
      $response = $client->request('GET', $form_state->getValue('url'));
    }
    catch (GuzzleException $e) {
      $form_state->setErrorByName('url', $this->t('The given URL is not valid'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->configFactory->getEditable(static::CONFIG)
      ->set('url', $form_state->getValue('url'))
      ->save();
    $form_state->setRedirect('pagespeed_viewer.index');
    parent::submitForm($form, $form_state);
  }

}
