<?php

namespace Drupal\pagespeed_viewer;

use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\pagespeed_viewer\Model\PageSpeedModel;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;

/**
 * Class PagespeedApiService.
 *
 * @package Drupal\pagespeed_viewer
 */
class PagespeedApiService {
  /**
   * Logger Service.
   *
   * @var Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $logger;

  /**
   * PagespeedApiService constructor.
   */
  public function __construct(LoggerChannelFactoryInterface $loggerChannelFactory) {
    $this->logger = $loggerChannelFactory->get('pagespeed_viewer');
  }

  /**
   * Queries measured pagespeed from the PageSpeed API.
   *
   * @param int $device
   *   Mobile or desktop.
   *
   * @return float
   *   Measured page speed for given device.
   * @throws GuzzleException
   */
  public function getPagespeed($device) {
    $client = new Client();
    $url = $this->getUrl();
    $response = $client->request('GET', 'https://www.googleapis.com/pagespeedonline/v5/runPagespeed?url='
                                                . $url
                                                . '&strategy='
                                                . $device);
    $responseObj = json_decode($response->getBody(), 1);
    $pagespeed = $responseObj['lighthouseResult']['categories']['performance']['score'];

    return $pagespeed * 100;
  }

  /**
   * Provides URL from config.
   *
   * @return string
   *   URL of configured page
   */
  public function getUrl() {
    $config = \Drupal::config('pagespeed_viewer.urlconfig');
    return $config->get('url');
  }

  /**
   * Provides pagespeed values and saves them into DB.
   *
   * @return array|bool
   *   Measured page speed.
   *   Returns FALSE if an error was occurred.
   */
  public function savePagespeed() {
    $desktop = 0;
    $mobile = 0;
    try {
      $desktop = $this->getPagespeed('desktop');
      $mobile = $this->getPagespeed('mobile');
      $model = new PageSpeedModel($desktop, $mobile);
      $avg = $model->calculateAverage();
      return [
        'desktop' => $desktop,
        'mobile' => $mobile,
        'avg' => $avg,
      ];
    }
    catch (GuzzleException $e) {
      $this->logger->error($e->getMessage());
      return FALSE;
    }
  }

}
