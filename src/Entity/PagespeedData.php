<?php

namespace Drupal\pagespeed_viewer\Entity;

use Drupal\Core\Entity\Entity;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityPublishedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\user\UserInterface;

/**
 * Defines the Pagespeed data entity.
 *
 * @ingroup pagespeed_viewer
 *
 * @ContentEntityType(
 *   id = "pagespeed_data",
 *   label = @Translation("Pagespeed data"),
 *   base_table = "pagespeed_data",
 *   entity_keys = {
 *     "id" = "id",
 *     "uuid" = "uuid",
 *     "uid" = "user_id",
 *     "url" = "url",
 *     "mobile" = "mobile",
 *     "desktop" = "desktop",
 *     "avg" = "avg",
 *     "created_at" = "created_at",
 *     "published" = "status",
 *   },
 * )
 */
class PagespeedData extends ContentEntityBase implements EntityPublishedInterface {

  use EntityChangedTrait;
  use EntityPublishedTrait;

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $currTime = \Drupal::time()->getCurrentTime();
    $formatedTimme = date('d/m/Y h:i', $currTime);
    $values += [
      'user_id' => \Drupal::currentUser()->id(),
      'created_at' => $formatedTimme,
    ];
  }

  /**
   * Getter.
   *
   * @return string
   *   URL of page.
   */
  public function getUrl() {
    return $this->get('url')->value;
  }

  /**
   * Setter.
   *
   * @param string $url
   *    URL of page.
   */
  public function setUrl($url) {
    $this->set('url', $url);
    return $this;
  }

  /**
   * Getter.
   *
   * @return float
   *   Mobile page speed.
   */
  public function getMobile() {
    return $this->get('mobile')->value;
  }

  /**
   * Setter.
   *
   * @param float $mobile
   *   Mobile page speed.
   */
  public function setMobile($mobile) {
    $this->set('mobile', $mobile);
    return $this;
  }

  /**
   * Getter.
   *
   * @return float
   *   Desktop page speed.
   */
  public function getDesktop() {
    return $this->get('desktop')->value;
  }

  /**
   * Setter.
   *
   * @param float $desktop
   *   Desktop page speed.
   */
  public function setDesktop($desktop) {
    $this->set('desktop', $desktop);
    return $this;
  }

  /**
   * Getter.
   *
   * @return float
   *   Average page speed.
   */
  public function getAvg() {
    return $this->get('avg')->value;
  }

  /**
   * Setter.
   *
   * @param float $avg
   *   Average page speed.
   */
  public function setAvg($avg) {
    $this->set('avg', $avg);
    return $this;
  }

  /**
   * Getter.
   *
   * @return string
   *   Desktop page speed.
   */
  public function getCreated() {
    return $this->get('created_at')->value;
  }

  /**
   * Getter.
   *
   * @return entity
   *   User entity
   */
  public function getOwner() {
    return $this->get('user_id')->entity;
  }

  /**
   * Getter.
   *
   * @return int
   *   User ID
   */
  public function getOwnerId() {
    return $this->get('user_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    // Add the published field.
    $fields += static::publishedBaseFieldDefinitions($entity_type);

    $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Authored by'))
      ->setDescription(t('The user ID of author of the Pagespeed data entity.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['url'] = BaseFieldDefinition::create('string')
      ->setLabel(t('URL'))
      ->setDescription(t('URL of the tested page'));

    $fields['mobile'] = BaseFieldDefinition::create('float')
      ->setLabel(t('Mobile'))
      ->setDescription(t('Pagespeed Mobile'));

    $fields['desktop'] = BaseFieldDefinition::create('float')
      ->setLabel(t('Desktop'))
      ->setDescription(t('Pagespeed Desktop'));

    $fields['avg'] = BaseFieldDefinition::create('float')
      ->setLabel(t('AVG'));

    $fields['created_at'] = BaseFieldDefinition::create('datetime')
      ->setLabel(t('Created'));

    return $fields;
  }

}
