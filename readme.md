CONTENTS OF THIS FILE
---------------------
 * Introduction
 * Requirements
 * Recommended modules
 * Installation
 * Configuration
 * Troubleshooting
 * FAQ
 * Maintainers

INTRODUCTION
------------
The PageSpeed Viewer module is a tool for measuring the loading speed of a
configured webpage using the PageSpeed Insights API. It also provides a statistics
page about the last results.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/pagespeed_viewer

 * To submit bug reports and feature suggestions, or track changes:
   https://www.drupal.org/project/issues/pagespeed_viewer

REQUIREMENTS
------------
This module requires the following modules:
 * 

INSTALLATION
------------

 * Install the Page Speed Viewer module as you would normally install a contributed
   Drupal module. Visit
   https://www.drupal.org/node/1897420 for further information.

CONFIGURATION
-------------
    1. Navigate to Administration > Extend and enable the module.
    2. If the module is enabled you can configure you URL navigating to
       Administration > Configuration > Web Services > Pagespeed Viewer.
    3. You can start the measurement navigating to Admin > Reports >
       Pagespeed Viewer.
    4. Navigating to navigating to Admin > Reports > Pagespeed Viewer > Statistics
       you can check your last results.  

MAINTAINERS
-----------
Bence Urszin (ubence) - https://www.drupal.org/u/ubence
